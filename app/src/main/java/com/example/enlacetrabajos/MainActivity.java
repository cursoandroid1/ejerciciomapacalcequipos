package com.example.enlacetrabajos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

   //1. declaramos
    Button botonInicial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //2. agregamos el elemento de la vista
        botonInicial = findViewById(R.id.botonInicial);



        //3. Ponemos a escuchar el boton
        botonInicial.setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext() , IndexPrincipal.class);
            startActivity(intent);
        });


    }


}